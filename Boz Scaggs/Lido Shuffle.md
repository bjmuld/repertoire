[Intro]

[v1 - minor emph]

    (minor)
    Lido missed the boat that day he left the shack
    But that was all he missed and he ain't comin' back
    A tombstone bar in a jukejoint car, he made a stop
    Just long enough to grab a handle off the top

[v2 - Maj. emph.]

    Next stop Chi town, Lido put the money down and let it roll
    He said one more job ought to get it
    One last shot 'fore we quit it
    One more for the road

[c1]

    Lido, whoa-oh-oh-oh
    He's for the money, he's for the show
    Lido's waitin' for the go
    Lido, whoa-oh-oh-oh
    He said one more job ought to get it
    One last shot 'fore we quit it
    One more for the road

[v3]

    Lido be runnin', havin' great big fun, until he got the note
    Sayin' toe the line or blow, and that was all she wrote

    (Maj.)
    He be makin' like a beeline, headin' for the borderline, goin' for broke
    Sayin' one more hit ought to do it
    This joint ain't nothin to it
    One more for the road

[c2]

    Lido, whoa-oh-oh-oh
    He's for the money, he's for the show
    Lido's waitin' for the go
    Lido, whoa-oh-oh-oh
    One more job ought to get it
    One last shot and we quit it
    One more for the road

[c3/bridge]

    Lido, whoa-oh-oh-oh
    He's for the money, he's for the show
    Lido's waitin' for the go
    Lido, whoa-oh-oh-oh...
