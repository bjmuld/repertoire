---
Title: Don't Tell Our Friends About Me
Author: Blake Mills
Key: G Maj
---

[verse]  

    | I - IV - | V - -  - |
    | I - IV - | V - -  - |
    | I - IV - | V - IV - |
    | V - IV - | I - IV - |
    | V - V7 - |

[chorus]  

    | I - IV - | V - - - | 

[interlude]  

    ?
------------------------------------------
[V1]

    The older I am, the wiser I'm not
    And I felt ashamed of how angry I got
    I know I was not getting my message across
    I know you can't stand it when that's how I talk
    When I summon a duel or when I brandish a thought
    I was wrong to turn honesty against you
    And sure, some of them could use a good talk

[C1]  

    But babe, don't tell all our friends about me
    Please, baby, don't tell our friends about me

[V2]

    I could sleep on the couch if you want me to
    Or I can be a good dog and just sit with you
    Do whatever it is that you ask me to do
    'Til you forgive me and I forgive you
    Yeah, I know you got the beating but I caught a few
    Frankly, I don't know what else I can confess to you
    Lovers may quarrel and spar sometimes

[C1]

    But babe, don't tell all our friends about me
    Please, baby, don't tell our friends about me

[Interlude]

    I know I fucked up (x8)
    But please, baby, don't tell all our friends about me

[V3]

    You said you just needed some time to adjust
    It's been 48 hours and 3 weeks and 2 months
    Hummingbirds hum and worker bees buzz
    You put too much confidence in the people you trust
    All the enemies and friends, they'll all tend to judge
    And I'll write songs that'll help me deal with issues
    And sure, some people may hear too much

[C2]
	
    But babe, they don't tell all our friends about us
    You know it makes them talk too much
    So please, baby, don't tell our friends about me
    (All your friends aren't my friends anymore)
    Please, baby, don't tell our friends about me

[Outro]