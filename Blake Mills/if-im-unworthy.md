# If I'm unworthy

## Artist: Blake Mills

### Key:  Db

### Tuning: open Db


[V 1]

    I've found a new meaning
    The oldest words in use
    Now I no longer ask myself
    What have I got to lose
    If I'm unworthy of the power I hold over you

[V 2]

    This line of thinking
    The wonders it can induce
    I'm twisted in my sheets now
    Look what your love can do
    What if I'm unworthy of the power I hold over you

[V 3]

    Time before was wasted
    With you,  life is not long enough
    I'll wrap you in my arms, babe
    I hope they'll be strong enough

[1 Verse Interlude / Solo]

[Verse 4]

    My time before was wasted
    With you life's just not long enough
    I'll wrap you in my arms, babe
    See if they're strong enough

[outtro]

    What if I'm unworthy of the power I hold over you
    What if I'm unworthy of the power I hold over you
    What if I'm unworthy of the power I hold over you
