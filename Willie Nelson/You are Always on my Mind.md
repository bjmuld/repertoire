#### You Are Always on My Mind
##### Willie Nelson

[Verse 1]

	D              A
	Maybe I didn't love you
	
	Bm       D          G            A
	Quite as often as I could have
	
	D                  A
	And maybe I didn't treat you
	
	Bm       D         Em7
	Quite as good as I should have
	
 

[C]

	G                  D
	If I made you feel second best
	
	G         D           Em
	Girl, I'm sorry I was blind
	
	A            Bm     A7    D     Em    F#m
	But you were always on my mind
	
	G        A7           D     G     A
	You were always on my mind

 

[Verse 2]

	D              A
	Maybe I didn't hold you
	
	Bm       D          G            A
	All those lonely, lonely times
	
	D                  A
	And I guess I never told you
	
	Bm       D         Em7
	I'm so happy that you're mine

 
[C2]

	G                  D
	Little things I should have said and done
	
	G         D           Em
	I just never took the time
	
	A            Bm     A7    D     Em    F#m
	You were always on my mind
	
	G        A7           D     G     A
	You were always on my mind
 
[Bridge]

	D     A  Bm    D
 
[Outro]

	D   A/Db  Bm  A
	Tell     me
	
	G                 D                 Em    G    A7
	Tell me that your sweet love hasn't died
	
	D     A  Bm
	Give     me
	
	D       G                  D              Em
	Give me one more chance to keep you satisfied
	
	     A             D
	I'll keep you satisfied
