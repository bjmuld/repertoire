#### Where Everybody Knows Your Name
##### Gary Portnoy and Judy Angelo

Key of D

[Metronome:]80/4

[v]
| I - V - | I - V - |
| I - V - | I - V - |

[pc]
| Vo - VI7 - | ii - - -| V7 - - -|

[c (key change)]
I - b7M - | IV - - - |
I - b7M - | IV - - - |
iii - IV - | iii - IV - | iii - IV V | I - V - | I - V - |

=================================================================================

[Verse 1]

    Making your way in the world today
    Takes everything you've got
    Taking a break from all your worries
    Sure would help a lot
    Wouldn't you like to get away?

[Chorus]

    Sometimes you want to go
    Where everybody knows your name
    And they're always glad you came
    You wanna be where you can see
    Our troubles are all the same
    You wanna be where everybody knows your name

[Verse 2]

    All those nights when you've got no lights
    The check is in the mail
    And your little angel hung the cat up by its tail
    And your third fiance didn't show

[Verse 3]

    Roll out of bed, Mr. Coffee's dead
    The morning's looking bright
    And your shrink ran off to Europe and didn't even write
    And your husband wants to be a girl
    Be glad, there's one place in the world

[Chorus]

[Outro]