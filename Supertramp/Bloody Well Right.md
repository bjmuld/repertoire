---
Title: Bloody Well Right
Artist: Supertramp
Key: Bb/C ?
Tempo: 120
---

## Bloody Well Right - Supertramp

### (Intro)

V1:

        So you think your schooling is phony
        I guess it's hard not to agree
        You say it all depends on money
        And who is in your family tree

Chorus:

        Right -- you're bloody well right
        You got a bloody right to say
        Right, you're bloody well right
        You know you got a right to say

        Ha-ha, you're bloody well right
        You know you're right to say
        Yeah, yeah, you're bloody well right
        You know you're right to say
        Me, I don't care anyway

### (Short Instrumental Verse )

V2:

        Write your problems down in detail
        Take them to a higher place
        You've had your cry, no, I shouldn't say wail
        In the meantime, hush your face

### ( Chorus )

        Bloody well right
        bloody well right
        bloody well right
        bloody well right

### ( Piano Solo )

Outtro:

        You got a bloody right to say
        You got a bloody right to say
        You got a bloody right to say
        You got a bloody right to say, yeah
