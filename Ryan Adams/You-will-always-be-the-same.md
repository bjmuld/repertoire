#### You Will Always Be The Same
##### Ryan Adams

[v]

	Go on to the street to the cars in the pouring rain
	Go on to the bus that left us in the dust and the flames
	And when the son meets the father
	It'll be something smarter for the pain

[c]

	But you will always be the same
	You will always be the same
	
[v]

	Go on little girl, feet twirl, go and make him smile
	Go on like the rumbling drums of the march of time
	And when the son meets the dad
	It'll be pretty bad for the pain

[c]

	But you'll always be the same
	You will always be the same
	You will always be the same

[v]

	Go on to the ones with the smoking guns in the heat
	Go on to the wars we won, they came home, they made up
	And when the father meets the son,
	And the blood makes us better than the gain

[c]
	You will always be the same 