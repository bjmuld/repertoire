#### Chin Up, Cheer Up
##### Ryan Adams

[key: C]

[v]

	| I - V - | I - V - | iv - iii - | IV - I -|
	
[c-A]

	| V - - - | I - - - | V - - - | I - - - |
	| V - - - | vi V IV - | vi V IV - | vi V - - | II7 - - -|
	(verse)

[v1]

	Took a walk with you
	In the shadow of my shoes
	Danced around the broken blues
	In the dirty summer rain
	Moonlight on the cars
	Parked in single file at bars
	With a thick and rosy smoke
	Waving its busted hand

[c1]

	Bringing you down, can't bring you down
	Bring you down, can't bring you down
	Bring you down, can't hear the sound
	Run through the river and into town
	Pretty little moon with it's head hung down
	Chin up. Cheer up.

[v2]

	I took a walk with you
	And I busted up my shoe
	In an old yellow canoe
	Out in Hollywood
	You called the police
	But they didn't care the lease
	On your place had run out
	Your landlord straightened his hat

[c2]

	Bringing you down, can't bring you down
	Bringing you down, can't bring you down
	Bringing you down, can't hear the sound
	Run through the river and into town
	Pretty little moon with it's head hung down
	Chin up. Cheer up. 