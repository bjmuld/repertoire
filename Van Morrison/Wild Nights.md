#### Wild Night
##### Van Morisson

[Key:] G
[Metronome:] 80/4

[v]

    As you brush your shoes
    Stand before the mirror
    And you comb your hair
    Grab your coat and hat
    
    And you walk, wet streets
    Tryin' to remember
    All the wild night breezes
    In your mem'ry ever

[pc]

    And ev'rything looks so complete
    When you're walkin' out on the street
    And the wind catches your feet
    Sends you flyin', cryin'

[c]

    Ooo-woo-wee!
    Wild night is calling, alright
    Oooo-ooo-wee!
    Wild night is calling

[v]

    And all the girls walk by
    Dressed up for each other
    And the boys do the boogie-woogie
    On the corner of the street

    And the people, passin' by
    Stare in wild wonder
    And the inside juke-box
    Roars out just like thunder

[pc]

    And ev'rything looks so complete
    When you walk out on the street
    And the wind catches your feet
    And sends you flyin', cryin'

[c]

    Woo-woo-wee!
    Wild night is calling
    Alright
    Ooo-ooo-wee!
    Wild night is calling, alright
    
    The wild night is calling
    The wild night is calling

[v]

    Come on out and dance
    Whoa, come on out and make romance
    Yes, indeed

    Come on out and dance
    Come on out, make romance

[Instrumental & horn solo]

[c]

    The wild night is calling, alright
    The wild night is calling

[v]

    Come on out an dance
    Yeah, come on out 'n make romance
    
    Come on out and dance, alright
    Come on out, n' make romance. 