# Love's in Need of Love Today
## Stevie Wonder

### key of EbM7

[Intro]
  
    I                  biio
    Good morn or evening friends

    ii                  V7
    Here's your friendly announcer

    Fm7             Ab/Bb         Ab7+    Gm7   Fm7   Ab/Bb
    I have serious news to pass on to.....every-bo....dy

    Eb7+              Eº
    What I'm about to say

    Fm7                     Ab/Bb
    Could mean the world's disaster

    Fm7                            Ab/Bb  Ab7+    Gm7      Fm7   Ab/Bb
    Could change your joy and laughter to tears.......and pain

    It's that...

    Eb7+        Cm7   Gm7
    Love's in need of love today

    Fm7
    Don't delay
                Cm7       Ab/Bb
    Send yours in right away

    Eb7+    Cm7
    Hate's goin' round

            Gm7
    Breaking many hearts

    Fm7
    Stop it please

                Cm7       Ab/Bb
    Before it's gone too far
  
[vocalize]

[Verse 2]

    The force of evil plans
    To make you its possession
    And it will if we let it
    Destroy ev....ery...bo...dy
    We all must take
    Precautionary measures
    If love and peace you treasure
    Then you'll hear me when I say
  
[Chorus]
