---
Title: Badfish
Artist: Sublime
Key: A Maj
Tempo: 90
---

## Badfish

V1:

        When you grab a hold of me
        You tell me that I'll never be set free
        
        But I'm a parasite
        Creep and crawl I step into the night

Prechorus:

        Two pints of booze
        Tell me are you a badfish, too?

Chorus 1:

        Ain't got no money to spend
        I hope the night will never end

        Lord knows I'm weak
        Won't somebody get me off of this reef?

V2:

        Baby, you're a big blue whale
        Grab the reef when all duck diving fails
        
        I swim but I wish I never learned
        The water's too polluted with germs

Prechorus 2:

        I dive deep when it's ten feet overhead
        Grab the reef underneath my bed

Chorus 2:

        Ain't got no quarrels with god
        Ain't got no time to grow old

        Lord knows I'm weak
        Won't somebody get me off of this reef?

### (Breakdown to Ac.Guitar)

### ( E.Guitar Solo )

Chorus 3:

        Ain't got no quarrels with God
        Ain't got no time to get old

        Lord knows I'm weak
        Won't somebody get me off of this reef?
