---
Title: The Maker
Artist: Daniel Lanois
Key: F
Tempo: 80
---

## The Maker - Lanois

V1:

        Oh  --  oh deep water
        Black   --  and cold like the night

        I stand with arms wide open
        I've run a twisted line

        I'm a stranger -  In the eyes of the maker

V2:

        I could not see -- for the fog in my eyes
        I could not feel -- for the fear in my life
        
        And from across the great divide
        In the distance I saw a light
        
        Jean Baptiste -  Walking to me with the maker

V3:

        My body  --  is bent and broken
        By a long and -- dangerous sleep

        I can't work the fields of Abraham
        And turn my head away

        I'm not a stranger  -  In the hands of the maker

### (Guitar Solo)

V4:

        Brother John -- have you seen the homeless daughters
        Standing there -- with broken wings

        I have seen -- the flaming swords
        There  -  over east of Eden

        Burning - in the eyes of the maker
        
        Burning - in the eyes of the maker
        
        Burning - in the eyes of the maker

### [     v - IV - I   jam ]