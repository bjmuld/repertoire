---
Title: Remedy
Artist: The Black Crowes
Key: Bb
Tempo: 120
---

## Remedy - The Black Crowes
 
V1:

        Baby, baby, why can't you sit still?
        Who killed that bird out on your windowsill?
        Are you the reason that it broke it's back?
        Tell me, did I see ya' baby laugh about that?

Refrain:

        If I come on like a dream
        Will you let me show you what I mean?
        Will you let me come on inside?
        Ooh, will you let it glide?

### Chorus:

        (Can I have some remedy?)       |   All I want is a remedy
        (Remedy for me, please)         |   For all of the things that I need
        (If I had some remedy)          |   Ooh, I would take enough
        (I'd take enough to please me)  |   To please me

V2:

        Baby, baby, why you dye your hair?
        Why you always keepin' with your mother's dare?
        So, baby, why who's who?   Who babe? I know you too
        Tell me, did the other children scold on you?

### (Refrain)

### (Chorus)

### (Guitar solo)

### Outtro Crescendo:

        I need a remedy, huh, yeah  |   For what is ailin' me    Ya' see?
        I need a remedy             |   For what is ailin'a me
        I need a remedy  yeah       |   For what's ailin' me

        If I only had a remedy

        You see I find it
        You see, baby, I want it
        Ooh, you see, I find it
        Ooh, I really want, I really want it
        Ya' see I need it

        I need a remedy, remedy, remedy, remedy, remedy, remedy
        Remedy, remedy, yeah
        Remedy - that's what I need
