---
Title: My Life
Author: Billy Joel
Key: D Maj
Tempo: 80
---

## My Life - Billy Joel

V1:

        Got a call from an old friend
        We used to be real close
        Said he couldn't go on the American way
        Closed the shop, sold the house
        Bought a ticket to the West Coast
        Now he gives them a stand-up routine in L.A.

### Chorus:

        I don't need you to worry for me 'cause I'm alright
        I don't want you to tell me it's time to come home
        I don't care what you say anymore, this is my life
        Go ahead with your own life, leave me alone

Bridge:

        I never said you had to offer me a second chance
        I never said I was a victim of circumstance
        I still belong, don't get me wrong
        And you can speak your mind, but not on my time

V2:

        They will tell you you can't sleep alone in a strange place
        Then they'll tell you you can't sleep with somebody else
        Ah, but sooner or later you sleep in your own space
        Either way it's OK, you wake up with yourself

### (Chorus - acapella)

### (Bridge)

### (Short Piano Interlude)

Refrain (just 2nd half):

        I don't care what you say anymore, this is my life
        Go ahead with your own life, leave me alone

Exit:

        Keep it to yourself it's my life.
        Keep it to yourself it's my life.
        Keep it to yourself it's my life.
        Keep it to yourself it's my life.

