---
Title: Wouldn't Want to Be Like You 
Artist: Alan Parsons Project
Key: Cm
Tempo: 80
---

## Wouldn't Want To Be Like You - Alan Parsons Project

 
V1:

        If I had a mind to
        I wouldn't want to think like you
        And if I had time to
        I wouldn't want to talk to you

### Chorus:

        I don't care
        What you do
        I wouldn't want to be like you

V2:

        If I was high class
        I wouldn't need a buck to pass
        And if I was a fall guy
        I wouldn't need no alibi...

### (Chorus)

### (Guitar Solo)

V3:

        Back on the bottom line
        Diggin' for a lousy dime
        If I hit a mother lode
        I'd cover anything that showed

### (Chorus)

### (Chorus)

### (Guitar Solo)

### (Dreamy Diatonic Vamp)

### (End)
