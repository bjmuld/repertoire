---
Title: Dreams
Artits: Fleetwood Mac
Key: G Maj
Tempo: 90
---

## Dreams

V1:
        [Bass, Drums, E.Piano, ghostly guitar]

        Now here you go again
        You say you want your freedom
        Well, who am I to keep you down?

        It's only right that you should
        Play the way you feel it

        But listen carefully to the sound
        Of your loneliness

Prechorus:

        Like a heartbeat... drives you mad
        In the stillness of remembering what you had
        And what you lost...
        And what you had...
        And what you lost

Chorus:

        [strummy guitar on chorus]

        Thunder only happens when it's raining
        Players only love you when they're playing
        Say... Women... they will come and they will go
        When the rain washes you clean... you'll know, you'll know

### (Instrumental Bridge)

V2:

        Now here I go again, I see the crystal visions
        I keep my visions to myself
        It's only me
        Who wants to wrap around your dreams and...
        Have you any dreams you'd like to sell?
        Dreams of loneliness...

### (Prechorus)

### (Chorus)

### (Chorus)

End on F