##### Takes Two
###### by Anders Osborne (key of A)

[verse]

	{| I - - - | IV - - -| I - - - | IV - - |}
	
[prechorus]

	{| iii - - - | IV - - - | iii - - - | IV - - - |}
	
[chorus]

	{| ii - - - | V7 - - - | I  - - - | IV - - - |
	 | I  - - - | IV - - - | ii - - - | V7 - - - |}
	
--------------------

[V1]

	I'm not sure
	If we should talk this way
	Its so easy to slip
	I try and watch what I say
	
	And it feels good
	to be here with you
	Ya and it feels right
	hope you feel it to

[PC1]

	We  hurt so long
	Don' know why
	We let it go on

[PC]

	Now I'm watchin' your every move	
	Am I feelin' to much feelin' enough
	
[c]
	
	It takes two to  be wrong
	It takes two to be right
	It takes two to understand
	Now I think I understand

[repeat intro]

[V2]

	You look strong
	I'm not sure what it is
	I know what I lost
	I can feel what I missed
	
	Were both growing again
	maybe learn when to quit
	I don't drink like I used to
	If you cant believe it

[PC2]

	You got me wrong
	Don't know why don't know why I let it go on

[C2]

	Now I'm watching your pretty face
	And I'm feeling to much Feeling enough
	
