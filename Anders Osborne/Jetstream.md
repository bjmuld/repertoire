##### Jetstream
###### by Anders Obsborne (key of D)

[Verse]
|I - - -|I - - -| IV - V - | I - - -|   (x2) 

[chorus]
|IV - V - | I - - -| IV - V - | I - - - |


	It's not as bad as you think
	I've got my head on straight
	Lost control but I took it back again
	Lord knows I went down
	But now I'm standing up
	Got what it takes, if it takes a lot of luck
	
	Jetstream, get back on your feet
	Jetstream, everyday and every week
	
	Somebody counted me out
	Somebody thought I was done
	Don't matter what they think I know they won't forget
	I'll go on like the wind
	I will burn like the sun
	Watch out baby I haven't even started yet
	
	Jetstream, get back on your feet
	Jetstream, everyday and every week
	
	You've got a choice, you can live, you can die
	It don't matter when, it don't matter why
	You've got a choice, live or die
	
	Jetstream, get back on your feet
	Jetstream, everyday and every week
	