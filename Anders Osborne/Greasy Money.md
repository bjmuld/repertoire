#### Greasy Money
##### by Anders Obsorne (key of A)
##### key of E

[metronome:]100/4

    [verse:]
    [I - - - | IV - - -| I - - -| I - - - |
    
    [prechorus:]
    |V - - -| I - IV -| V - - -| I - IV7 - |
    
    [chorus]
    |I - - -| V7 - - -| I - - -| I - - -|
    
--------------------

    Greasy money
    Ain't no tellin' where it's been
    Grease ya money
    It comes back to you again

    Ain't no missin' things you give away
    Give it up, put a smile on someone's face

    Greasy money
    It comes back to you again

    In times of wonder
    I give all my dimes away
    In times of wonder
    My guitar and me we'll play
    
    A heart of care, is a heart that doesn't mind
    What goes around, comes back to you in time

    Up yonder, greasy money comes your way

    Yeah easy money
    No you know there ain't no such a thing
    Easy money
    And it'll ruin everything

    You put it out it's bound to get back in
    A lazy man is no man full of sin
    
    Easy money, no there ain't no such a thing

	[instrumental verse]
	
    Greasy money
    You best keep your pocket full
    Greasy money
    Don't save a single bill
    
    When I'm lonesome, I spend it till it ends
    No money's good until it's greased by all your friends
    
    Greasy money, it comes back to you again
