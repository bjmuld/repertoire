#### Better Than
##### by Lake Street Dive (C)
##### Mood similar to "I can't make you love me" <br> rigid odd time... 9/8?

| C - - - | C - - - - -| C - - - | C - - - |
| C - - - | C - - - - -| C - - - | C - - - |
| V - - - | V - - - | vi - - - | vi - - - |
| IV - - - |

[v1]

    I could spend ages reading the news 
    I could spend days, singing the blues 
    But I turn up the TV light 
    Give up without a fight 
    Better than pretending to know what's wrong and what's right 

[v2]

    I could spend ages asking myself why 
    There's a million ways that I could say goodbye 
    But I turn down the lights 
    Come on baby come inside 
    Better then be-eing some fool's bride 
    Better than pretending to know what's wrong and what's right 

[instrumental]

    Turn down the lights 
    Come on baby, come inside 
    Better than pretending to know what's wrong and what's right 
    Better than be-eing some fool's bride 

    Well it's better than pretending to know what's wrong and what's right 
    Oh what's wrong, and what's right
    