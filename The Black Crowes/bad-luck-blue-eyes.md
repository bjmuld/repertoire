#### Bad Luck Blue Eyes Goodbye
##### by The Black Crowes (Gbm (A))

[verse]

	{|i   - - - | i  - - - | III - - - | III - - - |
	 |i   - - - | i  - - - | III - - - | III - - - |
	 |VII - - - | VI - - - | VII - - - | iv  - - - |
	   --first time only:
	 | III - - - | III - - - :|}

[pc]

	| VI -  -  - | III -  -   - |
	

[chorus]
	
	{|: III -  III7  - | VI - VI7 - :|}

---------------------------------------------

[v]

	With my winter time
	My idols and stage fright
	In another night
	Where the lights are loud and bright
	
	One dream from waking up saved
	Too shy to hold in the rage

[v]

	I know no luxury
	Of knowing what your eyes read
	I know one million ways
	To always pick the wrong thing to say
	
	A love that you never gave
	Always a time zone away

[pc]

	It's not out of spite
	I know what's right

[C]

	Bad luck blue eyes
	Goodbye
	( x 3 )

[v]

	Sometimes a memory
	Only sees what it wants to believe
	And what's filled in between
	Are days and nights that don't mean a thing

	Such a simple suicide
	A second chance never tried
	And you don't understand
	I need a helping hand

[c]

	Bad luck blue eyes
	Goodbye
	( x 5 )
	
	So you think that you've seen it all
	Is that a fact?
	So out your mouth a dictionary
	Spouts about this and that
	You got your do's, your don'ts
	Because and why
	I don't trust no one who don't
	Take their own advice
	
	Bad Luck Blue Eyes Goodbye