#### Heart Like a Levee
##### by Hiss Golden Messenger (key of G)

[verse]

	| IV - I - | V  - -  - |
	| IV - V - | vi - IV - |
	| IV - I - | V  - -  - |
	| V  - - - | vi - -  - |
	
[prechorus]

	||: V - - - | vi - IV - :||

=============================

[v1]

	Sweet May morning
	Lying in bed with nothing to say
	We'll pretend all we wanna
	Yeah, tomorrow I'll be on my way
    
	Sing me a river
	Go easy on me, I'm not doing too well
	Do you hate me, honey
	As much as I hate myself?

[c1]

	Heart like a levee
	I swing for the mountains in double-time
	Is it too heavy, honey?
	Did I carry my piece of the fire?

[prechorus]

	Standing in the wake
	with the sky still changing
	What's it going to take
	to keep you missing
	the rambling rake
	with a heart of obsidian?
	Standing in the wake
	with the sky still changing

[v3]

	Sing me a river
	I'm a peach tree jumper 
	with rain in my shoes
	If you let me, honey
	I'll set the world on fire for you
	
	Sing me a summer
	Oh, that Cincinnati moon
	like a wheel in the sky
	Shows two roads, honey
	Tell me which one leads to mine?
	
	Sing, little sister
	Be patient with me
	I don't have a rhyme
	Will you grieve me, honey?
	Did I give you a reason to try?

[prechorus]

	Standing in the wake with the sky still changing
	What's it going to take to keep you from taking this so serious?
	I know the dead are raging
	Standing in the wake - yes I've seen the changes

[chorus]

	Heart like a levee
	I swing for the mountains in double-time
	Do you hear me running?
	Did I carry my piece of the fire?
	