#### Biloxi
##### by Hiss Golden Messenger (key of A)

[Intro]

    ||: IV - - - | I - - - :||
    
[verse]

    || IV - - - | I - - - | V - - - | IV - I - ||
    
[chorus]

    || IV - - - | I - - - | V - - IV | I - - - | IV - - - | IV - - - | IV7 - - - |
    
-----------------------------------------------------------------

[V1]

    I was born on the Junco River
    Where the dragon surely dwells
    And Saint George stood with a mighty word
    And told it well

    Hey, babe, it's your birthday
    My sweet little one
    Six years old and truth be told
    You're the only one

[c1]

    It's hard, Lord
    Lord, it's hard
    It's hard, Lord
    Lord, it's hard
    Yes, everybody in the whole damn place has gotta have a good time
    Yeah, nah nah

[v2]

    Say Biloxi
    It's tough all over
    Ah, Houston
    You scared me sober

    But all around my old hometown I was known as a loner
    Oh you know I wasn't lonely, I just liked being alone

[c2]

    It's hard, Lord
    Lord, it's hard
    It's hard, Lord
    Lord, it's hard
    Oh, everybody in the whole damn place has gotta have a good time
    Yeah, nah nah nah nah

[inst verse]

[v3]

    I was down by the Junco River
    Where the dragon surely dwells
    And Saint George stood with a mighty word
    And told it well

[c3]

    It's hard, Lord
    Lord, it's hard
    It's hard, Lord
    Lord, it's hard
    There's one way in and there's one way out and we're gonna have a good time 