#### Space Captain
##### by Joe Cocker  (Cocker, Tedeschi in C)
In A (dropped D tuning):
  
[verse]

	|V - vi - | IV - I7 - |
	|V - vi - | IV7 - - - |

[chorus-intro]

	|I  - V/7 -| vi V IV - |

[chorus]

	|I7 - V7    -| I7 - V7 - |
-----------------------------

[intro]
[V1]

	(b7)
	Once I was traveling across the sky
	This lovely planet caught my eye
	And being curious I flew close by
	And now I'm caught here til I die

[Ci]

Until we die
[C1]

	Learning to live together x 4
	Till we die

[V2-inst]
[Ci-inst]
[C2-inst]
[V2]

	I lost my memory of where I've been
	We all forgot that we could fly
	Someday we'll all turn into peaceful men and women
	And we'll return into the sky

[Ci]
	Until we die

[Ci]
	Until we die

[Ci]
	Until we die

[C3]
	Learning to live together x 4
	Till we die
