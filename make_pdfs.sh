#!/usr/bin/env bash

shopt -s globstar

for mds in **/*.md; do

    filename="${mds%.md}"

    cmd="pandoc -f markdown_mmd+yaml_metadata_block -t pdf -o \"${filename}.pdf\" -s \"${mds}\" -V fontsize=13pt -V documentclass=extarticle -V geometry:top=0.6in -V geometry:left=2in -V geometry:bottom=0.6in -V papersize=legal"
    echo "${cmd}"
    eval "${cmd}"
done
