#### Gentle On My Mind
##### Recorded by Glen Campbell written by John Hartford

###### Key of DMaj7

[Verse]

	{| I  - - - | I  - - - | ii - - - | ii - - - |
	 | ii - - - | ii - V - | I  - - - | I  - - - |
	 
	 | I  - - - | I  - - - | I  - - - | ii - - - |
	 | ii - - - | ii - - - | ii - V - | ii - V - |
	 | I  - - - | I  - - - |}
	
---------------------------------------------------
[v1]

    It's knowing that your door is always open
    And your path is free to walk
    That makes me tend to leave my sleeping bag
    Rolled up and stashed behind your couch
    -
    And it's knowing I'm not shackled by forgotten words and bonds
    And the ink stains that have dried if on some line
    That keeps you in the back-roads
    By the rivers of my memory that keeps you ever gentle on my mind


    It's not clinging to the rocks and ivy
    Planted on the columns now that binds me
    Or something that somebody said
    Because they thought we fit together walking
	-
    It's just knowing that the world will not be cursing
    Or forgiving when I walk along some railroad track and find 
    That you are moving on the back-roads by the rivers of my memory
    And for hours you're just gentle on my mind


    Though the wheat fields and the clothes lines
    And the junkyards and the highways come between us
    And some other woman crying to her mother
    Cause she turned and I was gone
	-
    I still might run in silence tears of joy might stain my face
    And the summer sun might burn me till I'm blind
    But not to where I cannot see you walking on the back-roads
    By the rivers flowing gentle on my mind


    I dip my cup of soup back from some gurgling 
    Crackling caldron in some train yard
    My beard a roughning coal pile and
    A dirty hat pulled low across my face
	-
    Through cupped hands 'round a tin can
    I pretend I hold you to my breast and find
    That you're waving from the back-roads
    By the rivers of my memory ever smiling ever gentle on my mind
    